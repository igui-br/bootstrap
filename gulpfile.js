'use strict';

const del = require('del');
const autoprefixer = require('gulp-autoprefixer');
const csso = require('gulp-csso');
const gulp = require('gulp');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const stripCssComments = require('gulp-strip-css-comments');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
const pump = require('pump');

function clean() {
    return del(['./dist/*']);
}

function styles() {
    return gulp.src('./scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(stripCssComments())
        .pipe(gulp.dest('./dist'));
}

function optimize() {
    return gulp.src('./dist/*.css')
        .pipe(csso({
            restructure: true
        }))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./dist'));
}

function script(callback) {
    pump([
        gulp.src('./js/**/*.js'),
        gulp.dest('./dist'),
        babel({
            presets: [ '@babel/env' ]
        }),
        uglify(),
        rename({suffix: '.min'}),
        gulp.dest('./dist')
    ], callback);
}

const build = gulp.series(
    clean,
    gulp.parallel(
        gulp.series(styles, optimize),
        script
    )
);

exports.clean = clean;
exports.styles = styles;
exports.optimize = optimize;
exports.script = script;
exports.build = build;

exports.default = build;
